package main

import "fmt"

// Pobierz liczbę całkowitą z klawiatury. Wyswietl TAK lub NIE jezeli liczba jest parzysta lub nie jest
func main() {
	var number int

	for {
		fmt.Println("Podaj liczbe ")
		fmt.Scan(&number)
		check(number)
	}
}

func check(number int) {
	if number%2 == 0 {
		fmt.Println("TAK")
	} else {
		fmt.Println("NIE")
	}
}
