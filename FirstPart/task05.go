package main

import (
	"errors"
	"fmt"
)

// Oblicz (((a1+a2)*a3)-a4)/a5. Zmienne a1-a5 są typu zmiennoprzecinkowego. Uwzględnij komunikat przy dzieleniu przez zero.

func main() {
	result, err := count(getNumbers())

	if err == nil {
		fmt.Printf("result: %v\n", result)
	} else {
		fmt.Printf("err: %v\n", err)
	}
}

func getNumbers() [5]float32 {
	var a [5]float32
	index := 0

	for index < 5 {
		fmt.Println("Podaj liczbe ", index+1)
		fmt.Scan(&a[index])
		index++
	}

	fmt.Printf("a: %v\n", a)

	return a

}

func count(a [5]float32) (float32, error) {
	// (((a1+a2)*a3)-a4)/a5.
	if a[4] == 0 {
		return 0, errors.New("a5 cannot be zero!")
	} else {
		return (((a[0] + a[1]) * a[2]) - a[3]) / a[4], nil
	}
}
