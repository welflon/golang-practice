package main

import (
	"fmt"
	"os"
	"strings"
)

//Pobierz znak z klawiatury i zwróć informacje czy to samogłoska spółgłoska czy cyfra

func main() {
	var character string = string(loadCharacter())
	recognieseChar(character)
	fmt.Println("Podana wartość to ", recognieseChar(character))
}

func loadCharacter() []byte {
	var character []byte = make([]byte, 1)
	fmt.Println("Podaj znak ")
	os.Stdin.Read(character)
	return character
}

func recognieseChar(input string) string {
	var result string

	if isDigit(input) {
		result = "cyfra!"
	} else if isVowel(input) {
		result = "samogłoska!"
	} else {
		result = "spółgłoska!"
	}
	return result
}

// // samogłoska
func isVowel(input string) bool {
	var temporary = strings.ToLower(input)
	vowel := [6]string{"a", "e", "i", "o", "u", "y"}
	var result bool = false
	for _, element := range vowel {
		if element == temporary {
			result = true
		}
	}
	return result
}

func isDigit(input string) bool {
	digits := [10]string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
	var result bool = false
	for _, element := range digits {
		if element == input {
			fmt.Printf(input)
			result = true
		}
	}
	return result
}
