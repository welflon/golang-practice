package main

import (
	"fmt"
)

//Pobierz z klawiatury 3 nieujemne liczby całkowite. Znajdź najwieksza z nich. Wyśiwetl sume pozostałych tyle razy ile wynosi wartość najwiekszej liczby.

func main() {
	var number [3]int
	var counter = 0
	var max = 0
	var sum = 0

	for counter < 3 {
		fmt.Println("Podaj liczbe ")
		fmt.Scan(&number[counter])

		if number[counter] > 0 {
			if max < number[counter] {
				max = number[counter]
			}
			sum += number[counter]
			counter++
		}
	}
	counter = 0

	for counter < max {
		fmt.Print("sum x", counter)
		fmt.Println("  ", sum)
		counter++
	}

	fmt.Println("liczby ", number)
	fmt.Println("max ", max)
}
