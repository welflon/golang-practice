package main

import (
	"fmt"
)

//Pobierz 5 liczb z klawiatury i zwróć informację ile liczb z podanych było parzystych a ile nieparzystych

func main() {

	var counter int = countEvenNumbers(getNumbers())

	fmt.Printf("Podano %v liczb parzystych oraz %v liczb nieparzystych ", counter, 5-counter)

}

func getNumbers() [5]int {
	var numbers [5]int

	for i := 0; i < 5; i++ {
		fmt.Println("Podaj liczbe ", i+1)
		fmt.Scan(&numbers[i])
	}

	fmt.Printf("a: %v\n", numbers)

	return numbers
}

func countEvenNumbers(numbers [5]int) int {
	evenNumbers := 0
	for _, element := range numbers {
		if isEvenNumber(element) {
			evenNumbers++
		}
	}
	return evenNumbers

}

func isEvenNumber(number int) bool {
	return number%2 == 0
}
