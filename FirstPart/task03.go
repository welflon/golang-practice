package main

import "fmt"

//Pobierz liczbę całkowitą z klawiatury i sprawdź czy jest podzielna przez 3 i 5;  przez 3 (ale nie przez 5); przez 5 (ale nie przez 3), ani przez 5 ani przez 3

func main() {
	var number = loadNumber()
	divNumber := map[string]bool{
		"3": checkDivNumber(3, number),
		"5": checkDivNumber(5, number),
	}

	fmt.Println(divNumber)

}

func loadNumber() int {
	var number int
	fmt.Println("Podaj liczbe ")
	fmt.Scan(&number)
	return number
}

func checkDivNumber(div int, number int) bool {
	return number%div == 0
}
